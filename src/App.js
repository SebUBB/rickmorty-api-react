import './App.scss';
import Header from './components/Header/Header'
import Characters from './components/Characters/Characters'
import './components/Header/header.scss'

function App() {
  return (
    <>
      <Header
        title="Rick and Morty API"
      />
      <Characters />
    </>
  );
}

export default App;
