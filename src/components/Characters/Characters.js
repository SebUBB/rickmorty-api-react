import React, { useEffect, useState } from 'react'

const Characters = () => {

    const [character, setCharacter] = useState([])

    useEffect(() => {
        fetch('https://rickandmortyapi.com/api/character')
            .then(response => response.json())
            .then(data => setCharacter(data.results))
    }, [])

    return (
        <div className='container'>
            <div className='characters'>
                {
                    character.map(item => (
                        <div className='box' key={item.id}>
                            <img src={item.image} alt={item.name} />
                            <div className='char'>
                                <h3 className='name'>Name:{item.name}</h3>
                                <p className='specie'>Species:{item.species}</p>
                                <p className='status'>Status:{item.status}</p>
                                <p className='location'>Location:{item.location.name}</p>
                                <p className='gender'>Gender:{item.gender}</p>
                                <p className='origin'>Origin: {item.origin.name}</p>
                            </div>
                        </div>
                    ))
                }
            </div>
        </div>
    );
}

export default Characters;