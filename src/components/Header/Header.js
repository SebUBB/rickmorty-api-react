import React from 'react'
import './header.scss'

const Header = ({ title }) => {
    return (
        <header>
            <div className='container'>
                <div className='content'>
                    <h1 className='title'>{title}</h1>
                    <p className='text'>This project shows the characters from the famous Rick & Morty series.</p>
                </div>
            </div>
        </header>
    )
}

export default Header